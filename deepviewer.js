// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014

/**
 * @module client
 * @submodule deepviewer
 */

/**
 * DeepViewer
 * High Resolution Image Viewer using OpenSeadragon Library
 * Based on Multi-Res Image Viewer (zoom.js)
 * 
 * @class deepviewer
 */
var deepviewer = SAGE2_App.extend( {
	init: function(data) {
		// call super-class 'init'
		this.SAGE2Init("div", data);
                
        this.resizeEvents = "continuous";
        
        // reset
        this.viewer    = null;
		this.lastZoom  = null;
		this.lastClick = null;
		this.isShift   = null;
		this.dragging  = null;
		this.position  = null;
		
		// application specific 'init'
		this.element.id = "div" + data.id;
        this.element.style.background = "black";
		this.lastZoom  = data.date;
		this.lastClick = data.date;
		this.dragging  = false;
		this.isShift   = false;
		this.position  = {x:0, y:0};
		this.infinityMode  = false;
		this.wrap = false;
		this.source;
		
		// creates new OpenSeadragon viewer
		this.viewer = OpenSeadragon({
			id: this.element.id, 
			showNavigationControl: false,
			minZoomPixelRation: 0,
			maxZoomPixelRatio: Infinity,
			wrapHorizontal: false
		});

		// infinity zooming
		// this.viewer.maxZoomPixelRatio=Infinity;
		// this.viewer.minZoomPixelRation=0;

		this.controls.addButton({type: "prev", position: 1, identifier: "Left"});
		this.controls.addButton({type: "next", position: 7, identifier: "Right"});
		this.controls.addButton({type: "up-arrow", position: 4, identifier: "Up"});
		this.controls.addButton({type: "down-arrow", position: 10, identifier: "Down"});
		this.controls.addButton({type: "zoom-in", position: 12, identifier: "ZoomIn"});
		this.controls.addButton({type: "zoom-out", position: 11, identifier: "ZoomOut"});
		this.controls.addButton({type: "default", position: 2, label: "wrap", identifier: "Wrap"});
		this.controls.addTextInput({value: window.location.origin + "/uploads/highResImages/", identifier: "LoadResource", label: "Load:"});
		this.controls.finishedAddingControls();
        
        this.source = this.resrcPath + "example/earth.dzi";
        this.viewer.openTileSource(this.source);

        // if (this.state !== undefined && this.state !== null) {
        // 	this.load(this.state);
        // }else{
        // 	thisload();
        // }
	},
	
	/**
	* Load the app from a previous state
	*
	* @method load
	* @param state {Object} object to initialize or restore the app
	* @param date {Date} time from the server
	*/
	load: function(state, date) {
		console.log(this.state);
		if (this.state !== undefined && this.state !== null) {
			//opened from file manager
			this.viewer.openTileSource(window.location.origin + '/' + this.state);
		}else{
			//opened from application menu
			//open default image
			this.viewer.openTileSource(this.resrcPath + "example/earth.dzi");
		}

	},
	
	/**
	* Draw function, empty since handled by OpenSeadragon Viewer
	*
	* @method draw
	* @param date {Date} current time from the server
	*/
	draw: function(date) {
	},
	
	/**
	* Resize function, nothing to resize since handled by the parent
	*
	* @method resize
	* @param date {Date} current time from the server
	*/
	resize: function(date) {
		this.refresh(date);
	},

	/**
	* Handles event processing for the app
	*
	* @method event
	* @param eventType {String} the type of event
	* @param position {Object} contains the x and y positions of the event
	* @param user_id {Object} data about the user who triggered the event
	* @param data {Object} object containing extra data about the event
	* @param date {Date} current time from the server
	*/
	event: function(eventType, position, user_id, data, date) {

		if (eventType === "pointerPress" && (data.button === "left") ) {
			if ( (date - this.lastClick) < 350) {
				// double click
				if (this.isShift) {
					this.viewer.viewport.zoomBy(0.6);
				} else {
					this.viewer.viewport.zoomBy(1.4);					
				}
				this.viewer.viewport.applyConstraints();
				this.lastZoom = date;
			} else {
				// not a double click
				this.dragging = true;
			}
			// keep values up to date
			this.position.x = position.x;
			this.position.y = position.y;
			this.lastClick  = date;
		}
		// dragging picture - pan
		if (eventType === "pointerMove" && this.dragging ) {
            var delta = new OpenSeadragon.Point(this.position.x - position.x, this.position.y - position.y);
            this.viewer.viewport.panBy(
                this.viewer.viewport.deltaPointsFromPixels(delta)
            );
			this.position.x = position.x;
			this.position.y = position.y;
		}
		// dragging stopped stopped
		if (eventType === "pointerRelease" && (data.button === "left") ) {
			this.dragging = false;
			this.position.x = position.x;
			this.position.y = position.y;
		}

		// Scroll events for zoom
		if (eventType === "pointerScroll") {
			var amount = data.wheelDelta;
			var diff = date - this.lastZoom;
			if (amount >= 1 && (diff>300)) {
				// zoom in
				this.viewer.viewport.zoomBy(0.8);
				this.viewer.viewport.applyConstraints();
				this.lastZoom = date;
			}
			else if (amount <= 1 && (diff>300)) {
				// zoom out
				this.viewer.viewport.zoomBy(1.2);
				this.viewer.viewport.applyConstraints();
				this.lastZoom = date;
			}
		}


		if (eventType == "specialKey" && data.code == 37 && data.state == "down") {
			// left
			// pan left
			this.viewer.viewport.panBy(new OpenSeadragon.Point(-0.01, 0));
			this.viewer.viewport.applyConstraints();
		}
		else if (eventType == "specialKey" && data.code == 38 && data.state == "down") {
			// up
			//pan up
			this.viewer.viewport.panBy(new OpenSeadragon.Point(0, -0.01));
			this.viewer.viewport.applyConstraints();
		}
		else if (eventType == "specialKey" && data.code == 39 && data.state == "down") {
			// right
			// pan right
			this.viewer.viewport.panBy(new OpenSeadragon.Point(0.01, 0));
			this.viewer.viewport.applyConstraints();
		}
		else if (eventType == "specialKey" && data.code == 40 && data.state == "down") {
			// down
			// pan down
			this.viewer.viewport.panBy(new OpenSeadragon.Point(0, 0.01));
			this.viewer.viewport.applyConstraints();
		}else if (eventType == "specialKey" && data.code == 107 && data.state == "down") {
			// + down
			// zoom in
			this.viewer.viewport.zoomBy(1.1);
			this.viewer.viewport.applyConstraints();
		}
		else if (eventType == "specialKey" && data.code == 109 && data.state == "down") {
			// - down
			// zoom out
			this.viewer.viewport.zoomBy(0.9);
			this.viewer.viewport.applyConstraints();
		}else if (eventType == "specialKey" && data.code == 36 && data.state == "down") {
			// home down
			// default zoom
			this.viewer.viewport.goHome();
			this.viewer.viewport.applyConstraints();
		}else if (eventType == "specialKey" && data.code == 84 && data.state == "down") {
			// T
			// test
			console.log(this.viewer.preserveImageSizeOnResize);
		} else if (eventType === "widgetEvent") {
			switch (data.identifier) {
				case "Up":
					// up
					this.viewer.viewport.panBy(new OpenSeadragon.Point(0, -0.01));
					break;
				case "Down":
					// down
					this.viewer.viewport.panBy(new OpenSeadragon.Point(0, 0.01));
					break;
				case "Left":
					// left
					this.viewer.viewport.panBy(new OpenSeadragon.Point(-0.01, 0));
					break;
				case "Right":
					// right
					this.viewer.viewport.panBy(new OpenSeadragon.Point(0.01, 0));
					break;
				case "ZoomIn":
					// zoom in
					this.viewer.viewport.zoomBy(1.2);
					this.lastZoom = date;
					break;
				case "ZoomOut":
					// zoom out
					this.viewer.viewport.zoomBy(0.8);
					this.lastZoom = date;
					break;
				case "LoadResource":
					// load resource
					// this.displayText = text.split(" ")[0];
					this.source = data.text;
					this.viewer.openTileSource(this.source);
					break;
				case "Wrap":
					// toggles horizontal wraping
					this.wrap = !this.wrap;
					this.viewer.wrapHorizontal = this.wrap;
					this.viewer.openTileSource(this.source);
					break;
				default:
					console.log("No handler for:", data.identifier);
					return;
			}
			this.viewer.viewport.applyConstraints();

		}


		this.refresh(date);		
	},

	/**
	* Called when closing application
	* Empty since there is nothing to do at the end...
	*
	*/
	quit: function() {
    }


});


