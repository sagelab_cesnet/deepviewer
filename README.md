# Deep Viewer application #

## Description ##

Gigapixel image viewer for the SAGE2 wall. Based on OpenSeadragon javascript library.

## Features ##

* horizontal wrap mode for panoramatic and 360° images
* opens any local images from the SAGE2 server storage
* opens any remote images from the remote server (e.g., http://www.in2white.com )

## Installation ##

* copy deepviewer directory to ./public/upload/apps/
* create new directory ./public/upload/highResImages
* save .dzi image description files into this directory
* restart SAGE2 server

## Use ##

Controls:

* Left mouse button - pan
* Scroll - zoom

Hotkeys:

* Left - pan left
* Right - pan right
* Up - pan up
* Down - pan down
* +/- - zoom
* Home - default view and zoom

Widget:

* Load - load picture by .dzi or .xml file
* Wrap- toggle horizontal wrapping


Upcoming features:

* script for converting and uploading new images to the SAGE2 server
* opening images from the SAGE2 media browser

## Contact ##

Jiri Kubista, CESNET, Jiri.Kubista@cesnet.cz

![deep_viewer.jpg](https://bitbucket.org/repo/6aRGak/images/3460709359-deep_viewer.jpg)